import { NgModule } from '@angular/core';

import {
  MatMenuModule, MatIconModule, MatButtonModule,
  MatProgressSpinnerModule, MatExpansionModule, MatCardModule, MatSnackBarModule, MatPaginatorModule
} from '@angular/material';


const MAT_MODULES = [
  MatMenuModule,
  MatIconModule,
  MatButtonModule,
  MatProgressSpinnerModule,
  MatExpansionModule,
  MatCardModule,
  MatSnackBarModule,
  MatPaginatorModule
];

@NgModule({
  imports: MAT_MODULES,
  exports: MAT_MODULES,
})
export class MaterialModule { }
