import { Routes } from '@angular/router';
import { PeopleListComponent } from './components/list/people-list.component';

export const peoplesRoutes: Routes = [
  {
    component: PeopleListComponent,
    path: 'peoples'
  },
  {
    path: 'peoples/:id',
    component: PeopleListComponent
  }
];
