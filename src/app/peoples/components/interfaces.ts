import { Base } from 'src/app/base.interface';

export interface People extends Base {
  name: string;
  height: number;
  mass: string;
  hari_color: string;
  skin_color: string;
  eye_color: string;
  birth_year: string;
  gender: string;
  homeworld: string;
  films: string[];
}
