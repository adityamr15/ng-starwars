import { Component } from '@angular/core';
import { ApiService } from 'src/app/services/api-service.service';
import { People } from '../interfaces';
import { PageEvent } from '@angular/material';

@Component({
  selector: 'app-people-list',
  templateUrl: 'people-list.component.html',
  styleUrls: ['people-list.component.scss']
})

export class PeopleListComponent {
  list: People[];

  currentPageIndex = 0;

  paginationConfig: {
    count: number;
    next: string;
    previous: string;
  };

  constructor(private apiService: ApiService) {
    this.getList();
  }

  getList(endpoint = '/people', cb?: () => any) {
    this.apiService.getList<People>(endpoint).subscribe(res => {
      this.list = res.results;
      this.paginationConfig = {
        count: res.count,
        next: !!res.next ? res.next.split('/').pop() : null,
        previous: !!res.previous ? res.previous.split('/').pop() : null
      };

      if (typeof cb === 'function') {
        cb();
      }
    });
  }

  getPeopleID(peopleUrl: string): string {
    try {
      return peopleUrl.split('/')[5];
    } catch (error) {
      return '';
    }
  }

  onChangePage(event: PageEvent) {
    this.list = null;

    if ((event.pageIndex > this.currentPageIndex) && !!this.paginationConfig.next) {
      this.getList('/people' + this.paginationConfig.next, () => {
        this.currentPageIndex++;
      });
    } else if ((event.pageIndex < this.currentPageIndex) && !!this.paginationConfig.previous) {
      this.getList('/people' + this.paginationConfig.previous, () => {
        this.currentPageIndex--;
      });
    }
  }
}
