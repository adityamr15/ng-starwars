import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material.module';
import { RouterModule } from '@angular/router';

import { PeopleListComponent } from './components/list/people-list.component';

@NgModule({
  imports: [CommonModule, MaterialModule, RouterModule],
  declarations: [
    PeopleListComponent
  ],
})
export class PeoplesModule { }
