import { Component } from '@angular/core';
import { Film } from '../../interfaces';
import { ApiService } from 'src/app/services/api-service.service';

@Component({
  selector: 'app-film-list',
  templateUrl: 'film-list.component.html',
  styleUrls: ['film-list.component.scss']
})

export class FilmListComponent {
  list: Film[];

  constructor(private apiService: ApiService) {
    this.apiService.getList<Film>('/films').subscribe(res => {
      this.list = res.results;
    });
  }

  getMovieID(movieUrl: string): string {
    try {
      return movieUrl.split('/')[5];
    } catch (error) {
      return '';
    }
  }
}
