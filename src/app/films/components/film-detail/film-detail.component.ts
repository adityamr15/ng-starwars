import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api-service.service';
import { Film } from '../../interfaces';

@Component({
  selector: 'app-film-detail',
  templateUrl: 'film-detail.component.html',
  styleUrls: ['film-detail.component.scss']
})

export class FilmDetailComponent {
  movieDetail: Film;

  constructor(private activatedRoute: ActivatedRoute, private apiService: ApiService) {
    this.activatedRoute.paramMap.subscribe(params => {
      const movieID = params.get('id');
      if (!!movieID) {
        this.apiService.getDetail<Film>('/films/' + movieID).subscribe(res => {
          this.movieDetail = res;
        });
      }
    });
  }
}
