import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilmListComponent } from './components/film-list/film-list.component';
import { MaterialModule } from '../material.module';
import { RouterModule } from '@angular/router';
import { FilmDetailComponent } from './components/film-detail/film-detail.component';

@NgModule({
  imports: [CommonModule, MaterialModule, RouterModule],
  declarations: [
    FilmListComponent,
    FilmDetailComponent
  ]
})
export class FilmsModule { }
