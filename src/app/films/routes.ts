import { Routes } from '@angular/router';
import { FilmListComponent } from './components/film-list/film-list.component';
import { FilmDetailComponent } from './components/film-detail/film-detail.component';

export const filmRoutes: Routes = [
  {
    component: FilmListComponent,
    path: 'films'
  },
  {
    path: 'films/:id',
    component: FilmDetailComponent
  }
];
