import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';

import { environment } from 'src/environments/environment';

interface ListResponse<T> {
  next: string;
  previous: string;
  count: number;
  results: T[];
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient, private snackBar: MatSnackBar) { }

  getList<T>(context: string): Observable<ListResponse<T>> {
    const endpoint = `${environment.apiUrl}${context}`;
    return this.http.get<ListResponse<T>>(endpoint)
      .pipe(
        catchError<any, any>(err => {
          this.snackBar.open('Failed fetching data!', 'Error', { duration: 2000, verticalPosition: 'top' });
        })
      );
  }

  getDetail<T>(context: string): Observable<T> {
    const endpoint = `${environment.apiUrl}${context}`;
    return this.http.get<T>(endpoint)
      .pipe(
        catchError<any, any>(err => {
          this.snackBar.open('Failed fetching data!', 'Error', { duration: 2000, verticalPosition: 'top' });
        })
      );
  }
}
