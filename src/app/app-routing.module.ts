import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { filmRoutes } from './films/routes';
import { peoplesRoutes } from './peoples/routes';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'films',
    pathMatch: 'full'
  },
  ...filmRoutes,
  ...peoplesRoutes
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
